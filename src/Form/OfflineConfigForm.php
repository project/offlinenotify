<?php

namespace Drupal\offline_notify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\CronInterface;
use Drupal\Core\Mail\MailManagerInterface;

/**
 * Class OfflineConfigForm.
 */
class OfflineConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\CronInterface definition.
   *
   * @var \Drupal\Core\CronInterface
   */
  protected $cron;
  /**
   * Drupal\Core\Mail\MailManagerInterface definition.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $pluginManagerMail;

  /**
   * Constructs a new OfflineConfigForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
      CronInterface $cron,
    MailManagerInterface $plugin_manager_mail
    ) {
    parent::__construct($config_factory);
    $this->cron = $cron;
    $this->pluginManagerMail = $plugin_manager_mail;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
            $container->get('cron'),
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'offline_notify.offlineconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'offline_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('offline_notify.offlineconfig');

    $form['offline'] = array(
      '#type' => 'details',
      '#title' => $this->t('Offline'),
      '#open' => TRUE,
      '#description' => $this->t('Offline notify configuration'),
    );
    $form['offline']['enable_offline_notify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Offline Notify'),
      '#default_value' => $config->get('enable_offline_notify'),
    ];
    $form['offline']['offline_notifier'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Offline Notifier'),
      '#description' => $this->t('Please enter mail ids with comma seperate'),
      '#default_value' => $config->get('offline_notifier'),
      '#rows' => 5,
      '#states' => array(
        'visible' => array(
          ':input[name="enable_offline_notify"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="enable_offline_notify"]' => array('checked' => TRUE),
        ),
      ),
    ];
    $form['offline']['offline_mail_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail subject'),
      '#description' => $this->t('Please enter mail ids with comma seperate'),
      '#default_value' => $config->get('offline_mail_subject'),
      '#states' => array(
        'visible' => array(
          ':input[name="enable_offline_notify"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="enable_offline_notify"]' => array('checked' => TRUE),
        ),
      ),
    ];
    $form['offline']['offline_mailtemplate'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Offline Mail Template'),
      '#description' => $this->t('Please enter mail content'),
      '#default_value' => $config->get('offline_mailtemplate'),
      '#rows' => 15,
      '#states' => array(
        'visible' => array(
          ':input[name="enable_offline_notify"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="enable_offline_notify"]' => array('checked' => TRUE),
        ),
      ),
    ];
    $form['online'] = array(
      '#type' => 'details',
      '#title' => $this->t('Online'),
      '#open' => TRUE,
      '#description' => $this->t('Online notify configuration'),
    );
    $form['online']['enable_online_notify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Online Notify'),
      '#default_value' => $config->get('enable_online_notify'),
    ];
    $form['online']['online_notifier'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Online Notifier'),
      '#description' => $this->t('Please enter mail ids with comma seperate'),
      '#default_value' => $config->get('online_notifier'),
      '#states' => array(
        'visible' => array(
          ':input[name="enable_online_notify"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="enable_online_notify"]' => array('checked' => TRUE),
        ),
      ),
    ];
    $form['online']['online_mail_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail subject'),
      '#description' => $this->t('Please enter mail ids with comma seperate'),
      '#default_value' => $config->get('online_mail_subject'),
      '#states' => array(
        'visible' => array(
          ':input[name="enable_offline_notify"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="enable_offline_notify"]' => array('checked' => TRUE),
        ),
      ),
    ];
    $form['online']['online_mailtemplate'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Online Mail Template'),
      '#description' => $this->t('Please enter mail content'),
      '#default_value' => $config->get('online_mailtemplate'),
      '#rows' => 15,
      '#states' => array(
        'visible' => array(
          ':input[name="enable_online_notify"]' => array('checked' => TRUE),
        ),
        'required' => array(
          ':input[name="enable_online_notify"]' => array('checked' => TRUE),
        ),
      ),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('offline_notify.offlineconfig')
      ->set('enable_online_notify', $form_state->getValue('enable_online_notify'))
      ->set('online_notifier', $form_state->getValue('online_notifier'))
      ->set('online_mail_subject', $form_state->getValue('online_mail_subject'))
      ->set('online_mailtemplate', $form_state->getValue('online_mailtemplate'))
      ->set('enable_offline_notify', $form_state->getValue('enable_offline_notify'))
      ->set('offline_notifier', $form_state->getValue('offline_notifier'))
      ->set('offline_mail_subject', $form_state->getValue('offline_mail_subject'))
      ->set('offline_mailtemplate', $form_state->getValue('offline_mailtemplate'))
      ->save();
  }

}
